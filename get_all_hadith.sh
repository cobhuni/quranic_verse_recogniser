#!/bin/bash
#
#    get_all_hadith.sh
#
#######################


find /home/alicia/COBHUNI/development/corpus/sources/hadith_alislam/data/all \
    -type f \
    -name "*.json" \
    -exec jq -r '.original, (select(.commentary != null) | .commentary)' {} \; |
tr ' ' '\n' |
tee xxx_hadith_all_tokens |
grep . |
sort |
uniq -c |
sort -nr > xxx_hadith_all_types

