#!/usr/bin/env python3
#
#    quran_search.py
#
# find quranic citations in given input text
#
# to create the quran indexes, run:
#   $ python build_quran_index.py # creates quran_index.py
#
# dependencies:
#   * config.ini
#   * normaliser.py
#   * quran_index.py
#
# examples:
#   $ echo "أَنْعَمْتَ عَلَيْهِمْ غَيْرِ ٱلْمَغْضُوبِ" | python quran_searcher.py
#     ((1, 7) صِرَٰطَ ٱلَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ ٱلْمَغْضُوبِ عَلَيْهِمْ وَلَا ٱلضَّآلِّينَ)
#
# TODO
# ----
#   * change project name to quranic_verse_finder
#   * perform large scale testing against cobhuni corpora and texts that do not contain quran
#   * connect with twitter and check result (?)
#   * add frequency checker for each word in selector to reduce false positives (?)
#   * make spelling corrector better
#   * check if i can return the offset
#
####################################################################################################


import sys
from argparse import ArgumentParser, FileType
from itertools import chain
from functools import partial

from quran_index import quran_bigram_index, quran_length, quran_unigram_freq, \
                        quran_verse_len_one, quran_verse_len_two


#
# functions
#

def normaliser(text, allowed_charset):
    """ Discard all characters in text that are not in allowed_charset
        or are spaces, and retrieve splitted tokens.

    Args:
        text (str): text to normalise and split.
        allowed_charset (str): list of characters that are not to be removed.

    return:
        list: sequence of normalised and splitted tokens.

    """
    tokens = (''.join(filter(lambda c: c in allowed_charset, tok)) for tok in text.split())
    return [corrector(tok, allowed_charset) for tok in tokens]



def P(word, vocabulary):
    """ Calculate the probability of word.

    Args:
        word (str): word to calculate the probability for.
        vocabulary (dict): frequency list of unigrams in the quran.

    Return:
        float: Probability of word

    """
    N = sum(vocabulary.values())
    return vocabulary[word] / N if word in vocabulary else 0.0

def corrector(word, allowed_charset, vocabulary={k : v[0] for k,v in quran_unigram_freq.items()}):
    """ Calculate the most probable spelling correcttion for word.

    Args:
        word (str): Word to correct.
        allowed_charset (str): inventory of letters that won't be removed from text.
        vocabulary (dict): frequency list of unigrams in the quran.

    Return:
        str: Candidate word with the highest probability.

    """
    return max(candidates(word, allowed_charset), key=partial(P, vocabulary=vocabulary))

def candidates(word, allowed_charset):
    """ Generate possible spelling correctors for word.
    
    Args:
        word (str): Word to generate candidates.
        allowed_charset (str): inventory of letters that won't be removed from text.

    Return:
        set: Candidates of corrected word.

    """
    return (known([word]) or known(edits1(word, allowed_charset)) or known(edits2(word, allowed_charset)) or [word])

def known(words):
    """ The subset of words that appear in the quran.

    Args:
        words (list): list of words.

    Return:
        set: words that are in quran.

    """
    return set(w for w in words if w in quran_unigram_freq)

def edits1(word, allowed_charset):
    """ Calculate all edits that are one edit away from word.

    Args:
        word (str): word to calvulate the edits for.
        allowed_charset (str): inventory of letters that won't be removed from text.
    
    Return:
        set: return all possible edits.

    """
    letters    = allowed_charset
    splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    deletes    = [L + R[1:]               for L, R in splits if R]
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
    replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    inserts    = [L + c + R               for L, R in splits for c in letters]
    return set(deletes + transposes + replaces + inserts)

def edits2(word, allowed_charset): 
    """ Calculate All edits that are two edits away from word.

    Args:
        word (str): word to calvulate the edits for.
        allowed_charset (str): inventory of letters that won't be removed from text.
    
    Return:
        set: return all possible edits.

    """
    return (e2 for e1 in edits1(word, allowed_charset) for e2 in edits1(e1, allowed_charset))


def search(input_toks):
    """ Search input_toks in quran_bigram_index following the sequence of pairs.

    Args:
        input_toks (list): sequence of tokens to search in quran_bigram_index. Must have length>=3.

    Return:
        str, tuple: substring found along with its corresponding index.

    """
    fst, snd = [input_toks.pop(0) for _ in range(2)]

    if (fst, snd) not in quran_bigram_index:
        return None

    output_tok = [fst, snd]
    index_path = quran_bigram_index[(fst, snd)]

    while input_toks:
        trd = input_toks.pop(0)

        if (snd, trd) not in quran_bigram_index:
            if index_path:
                return output_tok, index_path

        index_next = index_path & quran_bigram_index[(snd, trd)]

        if not index_next:
            return output_tok, index_path

        index_path = index_next
        fst, snd = snd, trd
        output_tok.append(trd)

    if index_path:
        return output_tok, index_path


def get_matches(input_toks, min_len=3):
    """ Create substrings of input_toks of at least three characters, removing successively
    one character from the start (e.g.: from input_toks="abcde", yield "abcde", "bcde" and "cde")
    and check if they can be a part of a quranic verse.

    Args:
        input_toks(list): input text to parse.
        min_len(int): minimum length a matched substring must have.

    Yield:
        list, tuple: matched substring along with its Quran index

    """
    ABS_MIN_LEN = 3

    token_len = len(input_toks)

    if token_len >= ABS_MIN_LEN:

        for i in range(token_len-min_len+1):
        
            found = search(input_toks[i:])
            if not found:
                continue

            substr, index_gr = found

            if len(substr) < min_len:
                continue

            for index in index_gr:
                yield substr, index

def selector(matches, complete_verse=False):
    """ Select only the indexes of matched texts that have the same length of the verses
    (if complete_verse==True); or select the indexes corresponding to the longest matched text.

    Args:
        matches (list): sequence of lists of words and tuple containing corresponding
            sura,aya pair.
        complete_verse (bool): if False, select only the matches with verses having the same length   # VERSE IS COMPLETE
            as the matched text; if True, select the longest matched text.

    Yield:
        tuple: sura,aya index.

    """
    if not matches: return
    
    if complete_verse:
        yield from (i for s,i in matches if len(s)==quran_length[i])

    else:
        yield from (i for s,i in matches if len(s)==max(len(s) for s,i in matches))



def quran_searcher(text,
                   complete_verse,
                   max_freq_unigram=20,
                   max_freq_bigram=10,
                   min_length_unigram=4,
                   allowed_charset='النمويهربتكعفقسدذحجىخةشصضزءآثطغئظؤأإ'):
    """ Search input_text in the Quran.

    Args:
        text (str): input text to parse.
        complete_verse (bool): indicate if input_text constitutes a complete verse.
        max_freq_unigram (int): maximum frequency a token may have in Quran to be considered relevant.
            If frequency exceeds max_freq_unigram, then it won't be shown in results.
        max_freq_bigram (int): maximum frequency a bigram may have in Quran to be considered relevant.
            If frequency exceeds max_freq_bigram, then it won't be shown in results.
        min_length_unigram (int): when searching for a unique word in partial matches, the minimum
            length the word must have.
        allowed_charset (str): inventory of letters that won't be removed from text.

    Yield:
        tuple: sura,aya index.

    """
    tokens = normaliser(text, allowed_charset)
    tokens_length = len(tokens)

    # search as short verse
    if tokens_length == 1:

        tok = tokens[0]
        if tok in quran_verse_len_one:
            yield quran_verse_len_one[tok]

        elif not complete_verse:

            if len(tok) >= min_length_unigram and \
               tok in quran_unigram_freq and \
               quran_unigram_freq[tok][0] <= max_freq_unigram:

                    for index in quran_unigram_freq[tok][1]:
                        yield index

    elif tokens_length == 2:
        tok_pair = tuple(tokens)

        # for complete and partial, if the two words match a complete verse, return it
        FOUND = False
        if tok_pair in quran_verse_len_two:
            for index in quran_verse_len_two[tok_pair]:
                FOUND = True
                yield index

        if not FOUND and not complete_verse:
            if tok_pair in quran_bigram_index:
                indexes = quran_bigram_index[tok_pair]
                if len(indexes) <= max_freq_bigram:
                    for index in indexes:
                        yield index

    else:
        found = list(get_matches(tokens))

        if found:
            yield from selector(found, complete_verse)



if __name__ == '__main__':

    parser = ArgumentParser(description='find quranic citations in input text')
    parser.add_argument('infile', nargs='?', type=FileType('r'), default=sys.stdin, help='input text [DEFAULT stdin]')
    parser.add_argument('outfile', nargs='?', type=FileType('w'), default=sys.stdout, help='output sura,aya pairs [DEFAULT stdout]')
    parser.add_argument('--complete', action='store_true', help='input text is a complete verse')
    args = parser.parse_args()

    for index in quran_searcher(args.infile.read(), args.complete):
        print(index)

