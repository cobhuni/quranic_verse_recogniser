#!/usr/bin/env python3
#
# builddict.py
#
#######################################

from uthmani_quran import uthmani_quran


#DEBUG
#uthmani_quran = { 'a b c a': [(0, 1), (2, 3)],
#                  'x a c':   [(4, 5)]
#                }
#
#print('**debug:quran**', uthmani_quran, '\n') #DEBUG

quranIndex = {}
for verse, indexlist in uthmani_quran.items():
    #print('**debug:verse**', verse) #DEBUG

    words = verse.strip().split()
    #print('**debug:words**', words) #DEBUG

    uniqwords = set(words)

    for index in indexlist:
        #print('**debug:index**', index) #DEBUG
        
        for word in uniqwords:
            #print('**debug:word**', word) #DEBUG
            
            nextwords = [(words[i+1] if i<len(words)-1 else '0') for i,w in enumerate(words) if w==word]

            quranIndex[word] = quranIndex.get(word, [])+[index, nextwords]
            #quranIndex[word] = {**quranIndex.get(word,{}), **{index : nextwords}} #DEBUG

from pprint import pprint as pp
pp(quranIndex)

            
# INPUT:
# 'a b c a': [(0, 1), (2, 3)],
# 'x a c':   [(4, 5)],
#
#
# OUTPUT:
#   word  index   nextwords
# { a : [ (0,1) , [b, 0],
#         (2,3) , [b, 0],
#         (4,5) , [c]
#       ],
#   b : [ (0,1) , [c],
#         (2,3) , [c]
#       ],
#   c : [ (0,1) , [a],
#         (2,3) , [a],
#         (4,5) , [0]
#       ],
#   x : [ (4,5) , [a]
#       ]
# }


#TODO include normalization keeping variations
#
# a_norm : [ a1 : {...}
#            a2 : ...
#          ]

