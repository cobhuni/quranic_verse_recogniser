#!/usr/bin/env python
#                          
#     test_verse_recogniser.py
#
# Tester for verse_recogniser.py
#
# usage:
#   $ python test_verse_recogniser.py
#
#   To run only custom tests:
#   $ MAX=$(jq 'keys | length' < goldstandard_custom.json)
#   $ python -m unittest $(for ((i=1; i<=$MAX; i++)) ; do echo -n "test_verse_recogniser.TestVerseRecogniser.test_custom_$i " ; done)
#
#############################################################################

import os
import sys
import json
import unittest

from functools import partial

from verse_recogniser import VerseRecogniser


def create_test(name, text, i_expected):
    """ Auxiliary routine to create seperate tests
        using one test function.
    
    Args:
        name (str): Name of the test to duplicate.
        text (str): Text to test.
        i_expedted (list): Expected result to check.

    Returns:
        function: New test created.

    """
    def test(obj):
        getattr(obj, name)(text, i_expected)
    return test

class TestVerseRecogniser(unittest.TestCase):

    MYPATH = os.path.dirname(os.path.realpath(__file__))

    with open(os.path.join(MYPATH, 'goldstandard_custom.json')) as fp:
        testdata_custom = json.load(fp)

    with open(os.path.join(MYPATH, 'goldstandard_quran1.json')) as fp:
        testdata_quran1 = json.load(fp)

    with open(os.path.join(MYPATH, 'goldstandard_quran2.json')) as fp:
        testdata_quran2 = json.load(fp)

    with open(os.path.join(MYPATH, 'goldstandard_quran3.json')) as fp:
        testdata_quran3 = json.load(fp)

    with open(os.path.join(MYPATH, 'fake_verses.txt')) as fp:
        testdata_fake = fp.readlines()

    def _test_custom(self, text, ind_expected):
        """ Check if the indexes returned by getIndex when text is passed
            are included in ind_expected.
    
        Args:
            text (str): Text to test.
            ind_expected (list): Group of 2-element tuple indexes in which
                the result of getIndex should be included.
        
        """
        ind_result = VerseRecogniser.getIndex(text)
        self.assertTrue(any(x for x in ind_expected if x in ind_result))

    #FIXME dejar uno con pocos ejemplo
    def test_quran1(self):
        """ Check if all verses from the simple version of the quran taken form
            tanzil.net are recognized.

        """
        input_texts = TestVerseRecogniser.testdata_quran1['texts']
        indexes = TestVerseRecogniser.testdata_quran1['verses']

        partial_getIndex = partial(VerseRecogniser.getIndex, subtext=False)  #FIXME poner a true (quitar)
        results = VerseRecogniser.getIndexList(input_texts, partial_getIndex, parallel=True)
        self.assertTrue(all(any(x for x in exp if x in got) for exp,got in zip(indexes, results)))

    def test_quran2(self):
        """ Check if all verses from the quran taken form altafsir.com are recognized.
    
        """
        input_texts = TestVerseRecogniser.testdata_quran2['texts']
        indexes = TestVerseRecogniser.testdata_quran2['verses']
    
        partial_getIndex = partial(VerseRecogniser.getIndex, subtext=False)
        results = VerseRecogniser.getIndexList(input_texts, partial_getIndex, parallel=True)
        self.assertTrue(all(any(x for x in exp if x in got) for exp,got in zip(indexes, results)))
    
    def test_quran3(self):
        """ Check if all verses from the quran taken form altafsir.com are recognized.
    
        """
        input_texts = TestVerseRecogniser.testdata_quran3['texts']
        indexes = TestVerseRecogniser.testdata_quran3['verses']
    
        partial_getIndex = partial(VerseRecogniser.getIndex, subtext=False)
        results = VerseRecogniser.getIndexList(input_texts, partial_getIndex, parallel=True)
        self.assertTrue(all(any(x for x in exp if x in got) for exp,got in zip(indexes, results)))

    def test_false_messiah(self):
        """ Check any of the fake verses is recognised as quran.
    
        """
        input_texts = TestVerseRecogniser.testdata_fake
    
        partial_getIndex = partial(VerseRecogniser.getIndex, subtext=True)
        results = VerseRecogniser.getIndexList(input_texts, partial_getIndex, parallel=True)
        self.assertTrue(all(not ind for ind in results))


# create separate tests for all examples in goldstandard_custom
for i, pair in enumerate(TestVerseRecogniser.testdata_custom.items(), 1):
    text, ind_exp = pair
    setattr(TestVerseRecogniser, "test_custom_%d" % i, create_test("_test_custom", text, ind_exp))

if __name__ == '__main__':
    unittest.main()

