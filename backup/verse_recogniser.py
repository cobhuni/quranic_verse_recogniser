#!/usr/bin/env python
#
#     verser_recogniser.py
#
# Given a stream of text, it returns the sura and aya indexes of each line of text.
# It can handle non exact matches of text and not complete ayas.
#
# In plain text output, if a text is not found in the Quran, it returns the index -1,-1.
#
# Beaware that there are 94 distinct verses that appear more than once in the Quran.
# Because of that, and because you can search a verse with the partial content of
# the text, the result may include more than one index.
#
#   INPUT    OUTPUT in plain text    OUTPUT in json
#   -----    --------------------    --------------
#   text1    i:j                    [{'text'  : text1,               
#   text2    n:m o:p                  'index' : [{'sura':i, 'aya':j}]
#   text3    -1:-1                   }, 
#                                    {'text'  : text2,
#                                     'index' : [{'sura':n, 'aya':m},
#                                                {'sura':o, 'aya':p}]
#                                    },
#                                    {'text'  : text3,
#                                     'index' : [],
#                                    }]                         
#
# Example:
#   $ echo خَٰلِدِينَ فِيهَا لَا يُخَفَّفُ عَنْهُمُ | python verse_recogniser.py --parallel -sp
#   $ echo foo | python verse_recogniser.py -sp --parallel --json
#
#   Sorting result:
#   $ echo الرحمن | python verse_recogniser.py -sp --json | jq '.[].verse | sort_by(.sura,.aya)'
#
# TODO
# ====
#  * AlrHmn with -sp is not found. This is because it is written with 2 ra letters in the version
#    in skeleton_quran. If it comes in a larger partial search, this won't be found. Think more
#    about this.
#  * Perhaps it would be useful to add an argument to choose btw ratio() and quick_ratio(). ??
#  * In 3-5 word non-quranic sentences, with -sp option, there are false positives (in every 10,000
#    there are less than a dozen cases). The problem with input texts is that they are either too short,
#    or it contains too many function words, or both. A way of solving this could be to include a
#    --strict param that filters with a list of function words all dibuous matchings and reports them.
#  * Possibility of adding a param to process all lines of text in block and try to find quranic
#    references using n-grams. In orther for this to work properly the accuracy should be very good.
#    Probably all previous todo poings should have been resolved.
# 
#                                                                    احفظ الرمز يا كبيكج يا كبيبج
######################################################################################################

import sys
import json
import re

from argparse import ArgumentParser,FileType
from difflib import SequenceMatcher
from itertools import chain,tee
from functools import partial
from multiprocessing import Pool

from uthmani_quran import uthmani_quran
from skeleton_quran import skeleton_quran


class VerseRecogniser:
    """ Searches exact or non-exact quranic verses and collect the indexes
        of the matching texts.

    """
    def _simplifyscript(text):
        """ Simplify script in text to a meaningless skeleton structure.

        Args:
            text (str): Text to modify.

        Returns:
            str: Modified version of text.
    
        """
        # remove wasl from alif
        text = text.replace('ٱ', 'ا')
    
        # remove alif khanjariyya
        text = text.replace('ٰ', '')
    
        # remove alif
        text = text.replace('ا', '')
    
        # remove alif maqsura (convert into ya and remove)
        text = text.replace('ى', '')
    
        # remove ya
        text = text.replace('ي', '')
        
        # convert Sad + small sin letter above (0x6dc) into sin
        text = text.replace('صۜ','س')
    
        # replace small high Nun my plain nun
        text = text.replace(chr(0x6e8),  chr(0x646))
    
        # remove diacritics
        for v in {'ً' : 'fathatan',
                  'ٌ' : 'dammatan',
                  'ٍ' : 'kasratan',
                  'َ' : 'fatha',
                  'ُ' : 'damma',
                  'ِ' : 'kasra',
                  'ْ' : 'sukun'}:
            text = text.replace(v, '')
    
        # remove rare quranic letters
        for c in {'ٓ' : 'arabic maddah above',  # U+0653
                  'ۡ' : 'small high dotless head of khah', # U+6e1
                  '۟' : 'small sukun', # U+6DF
                  'ۢ' : 'small mim letter above', # U+6E2
                  'ۥ' : 'small waw',  # U+6E5
                  'ۦ' : 'small ye mardooda',  # U+6E6
                  'ٔ' : 'isolated hamza above',  # U+654
                  'ـ' : 'tatweel/kashida',  # U+640
                  'ۭ' : 'small mim below',  # U+6ED
                  '۠' : 'small rectangular zero above',  # U+6E0
                  '۪' : 'empty centre stop below',  # U+6EA
                  '۫' : 'empty centre stop above',  # U+6EB
                  '۬' : 'rounded stop with filled centre above',  # U+6EC
                  'ۣ' : 'small sin letter below',  # U+6E3
                  'ۤ' : 'small high madda'  # U+6E4
                  }:
            text = text.replace(c, '')
    
        # remove waw
        text = text.replace('و', '')
    
        # remove hamzas
        for h in {'ء' : 'hamza',
                  'آ' : 'madda',
                  'أ' : 'alif with hamza above',
                  'إ' : 'alif with hamza below',
                  'ؤ' : 'waw with hamza above',
                  'ئ' : 'ya with hamza above',
                  'ٴ' : 'arabic letter high hamza',
                 }:
            text = text.replace(h, '')
    
        # join to next word all isolated yas or has
        text = re.sub(r'((?: +|^)[يه]) +', r'\1', text)
    
        # remove isolated hamza at the beginning
        text = re.sub(r'^ *ّ', '', text)
    
        # convert shadda into previous consonant
        text = re.sub(r'(.)ّ', r'\1\1', text)
    
        # reduce 4 equal consonants to three
        text = re.sub(r'(.)\1\1\1+', r'\1\1\1', text)
    
        # reduce 3 lams to 2
        text = text.replace('للل', 'لل')
    
        # join ibn ummi
        text = text.replace(' بن مم ', ' بنمم ')
    
        # remove nun at beginning if folowed by two lams 72:16
        text = text.replace('ن لل ', 'لل ')
    
        # convert ta marbuta into ta maftuha
        text = text.replace('ة', 'ت')
    
        # normalise spaces
        text = re.sub(r'\s+', r' ', text)
    
        return text.strip()

    def _similarMatch(verse, text_simple, ratio=0.94):
        """ Check similarity of verse and text_simple and decide
            if they match.

        Args:
            verse (str): Text of verse from one of the qurans
            text_simple (str): Simplified text to search.
            ratio(float): Ratio level to consider similar search equal.

        Returns:
            bool: True if verse and text_simple are considered to match,
                  False otherwise.

        """
        verse_simple = VerseRecogniser._simplifyscript(verse)
        
        if verse_simple == text_simple:
            return True

        seqMat = SequenceMatcher(None, verse_simple, text_simple).quick_ratio() #DEBUG ratio()

        if seqMat > ratio:
            return True

        return False

    def getIndex(text, ratio=0.94, similar=True, subtext=True, subtext_only=False):
        """ Search text in the verses of the Quran and give
            a list of sura:aya matching indexes.

        Args:
            text (str): Text to search.
            ratio(float): Ratio level to consider similar search equal.
            similar (bool): Do not only an exact search but a similar too.
            subtext (bool): If True, text may be a subtext of a verse.
            subtext_only (bool): If True, search text *only* as a subtext of a verse.

        Return:
            list: Matched sura:aya indexes, or empty list if no matches.
        
        """
        if similar or subtext_only:
            text_simple = VerseRecogniser._simplifyscript(text)

        # 1. search of complete verse match
        if not subtext_only:

            # 1.1 complete and exact
            if text in uthmani_quran:
                return uthmani_quran[text]

            # 1.2 complete and similar
            if similar:

                if text_simple in skeleton_quran:
                    return skeleton_quran[text_simple]

                matches = list(skeleton_quran[v] for v in skeleton_quran if VerseRecogniser._similarMatch(v, text_simple, ratio))
                if matches:
                    return list(chain(*matches))

        if not subtext and not subtext_only:
            return []

        # 2. search for subtext match

        # 2.1 subtext and exact
        matches = [uthmani_quran[v] for v in uthmani_quran if text in v]
        if matches:
            return list(chain(*matches))

        if not partial:
            return []

        # 2.2 subtext and similar
        if similar:

            # input text is too ambiguous
            if not text_simple.strip():
                return []

            matches = [skeleton_quran[v] for v in skeleton_quran if text_simple in v]
            if matches:
                return list(chain(*matches))       

        return []


    def getIndexList(texts, funct=getIndex, parallel=False, workers=4):
        """ Search the index of each text item of texts.

        Args:
            texts (iter): Texts to check.
            partialFunct (function): Callable object.
            parallel (bool): If True, run the searches in parallel. If False, run linearly.
            workers (int): Number of worker processes (only if parallel is set True).

        Returns:
            list: Groups of indexes corresponding to each line of text.

        """
        # muliprocessing execution
        if parallel:
    
            pool = Pool(processes=workers)
            indexes = pool.map(funct, texts) # [[[i, j]], [[k, l], [m, n]], [], ...]
            pool.close()
            pool.join()
    
        # linear execution
        else:
            indexes = list(map(funct, texts)) #DEBUG ineficiente

        return indexes


if __name__ == '__main__':

    parser = ArgumentParser(description='given some text from stdin prints into stdout numbers of sura and aya')
    parser.add_argument('infile', nargs='?', type=FileType('r'), default=sys.stdin, help='input file to parse [DEFAULT stdin]')
    parser.add_argument('outfile', nargs='?', type=FileType('w'), default=sys.stdout, help='output file to create [DEFAULT stdout]')
    parser.add_argument('--similar','-s', action='store_true', help='similar match instead of exact')
    parser.add_argument('--subtext','-p', action='store_true', help='input text may be a subtext from a verse')
    parser.add_argument('--subtext_only', action='store_true', help='search text only as a subtext of a verse')
    parser.add_argument('--json', action='store_true', help='input and output in json format')
    parser.add_argument('--ratio', '-r', type=float, default=0.94, help='ratio to accept equality in similar search [DEFAULT 0.94]')
    parser.add_argument('--parallel', action='store_true', help='run in parallel')
    parser.add_argument('--workers', '-w', type=int, default=4, help='number of worker processes [DEFAULT 4]')
    args = parser.parse_args()

    input_texts = (re.sub(r'\s+', r' ', l.strip()) for l in args.infile)
    
    if args.json: input_texts, textcopy = tee(input_texts)

    partial_getIndex = partial(VerseRecogniser.getIndex, ratio=args.ratio, similar=args.similar,
                                                         subtext=args.subtext, subtext_only=args.subtext_only)

    indexes = VerseRecogniser.getIndexList(input_texts, partial_getIndex, args.parallel, args.workers)

    # output in json
    if args.json:
    
        outdata = [{'text':text, 'verse': list({'sura' : i[0], 'aya' : i[1]} for i in ind)}
                                                       for text, ind in zip(textcopy, indexes)]
    
        json.dump(outdata, args.outfile, ensure_ascii=False)
        
    # output in plain text
    else:
    
        for ind in indexes:
            if not ind:
                ind = [(-1,-1)]
            print(' '.join('%d:%d' % (i,j) for i,j in ind), file=args.outfile)

