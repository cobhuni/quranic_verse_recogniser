#!/usr/bin/env python3
#
# builddict_v2.py
#
# create efficient version of quran
#
# usage:
#   $ python builddict_v2.py > quran_index.py
#
#############################################

from uthmani_quran import uthmani_quran as input_quran


#input_quran = {
#   'a b c x' : [(0,1), (4,5)],
#   'e a b c' : [(7,2)],
#   'a b b'   : [(8,8)],
#   'a c c b' : [(2,3)],
#   'c a b'   : [(6,2)]
#}

#output_quran = {
#   ('a', 'b') : {(0,1), (4,5), (7,2), (8,8), (6,2)} ,
#   ('b', 'c') : {(0,1), (4,5), (7,2)} ,
#   ('c', 'x') : {(0,1), (4,5)} ,
#   ('e', 'a') : {(7,2)} ,
#   ('b', 'b') : {(8,8)} ,
#   ('a', 'c') : {(2,3)} ,
#   ('c', 'c') : {(2,3)} ,
#   ('c', 'b') : {(2,3)} ,
#   ('c', 'a') : {(6,2)}
#}

output_quran = {}

for verse, index_list in input_quran.items():

    words = verse.strip().split() #TODO add more normalisation steps
    index_list = set(index_list)

    for i in range(len(words)-1):
        pair = (words[i], words[i+1])

        output_quran[pair] = output_quran.get(pair, set()) | index_list

from pprint import pprint
pprint(output_quran)

