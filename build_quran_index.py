#!/usr/bin/env python3
#
#    build_quran_index.py
#
# Create index of word transitions for quran-simple.txt,
# index of verse lengths and struct containing short verses
#
# dependencies:
#   quran-simple.txt # downloaded from http://tanzil.net/download/
#   config.ini
#
# usage:
#   $ python build_quran_index.py
#
##############################################################

#input:
#0|1|a b c x
#4|5|a b c x
#7|2|e a b c
#8|8|a b b
#2|3|a c c b
#6|2|c a b

#output:
#{
#   ('a', 'b') : {(0,1), (4,5), (7,2), (8,8), (6,2)} ,
#   ('b', 'c') : {(0,1), (4,5), (7,2)} ,
#   ('c', 'x') : {(0,1), (4,5)} ,
#   ('e', 'a') : {(7,2)} ,
#   ('b', 'b') : {(8,8)} ,
#   ('a', 'c') : {(2,3)} ,
#   ('c', 'c') : {(2,3)} ,
#   ('c', 'b') : {(2,3)} ,
#   ('c', 'a') : {(6,2)}
#}

import os
from configparser import ConfigParser
from itertools import groupby
from pprint import pprint

CONFIG_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini'),

def normaliser(text, allowed_charset='النمويهربتكعفقسدذحجىخةشصضزءآثطغئظؤأإ'):
    """ Discard all characters in text that are not in allowed_charset
        or are spaces, and retrieve splitted tokens.

    Args:
        text (str): text to normalise and split.
        allowed_charset (str): list of characters that are not to be removed.

    return:
        list: sequence of normalised and splitted tokens.

    """
    text_norm = (c for c in text.strip() if c in allowed_charset or c == ' ')
    tokens = (''.join(tok) for key,tok in groupby(text_norm, key=lambda x: x!=' '))
    return list(filter(str.strip, tokens))

with open('quran-simple.txt') as infp:

    cfg = ConfigParser(inline_comment_prefixes=('#'))
    cfg.optionxform = str # make parser case sensitive
    cfg.read(CONFIG_PATH)

    lines = filter(None, (l.strip() for l in infp))
    entries = [l.split('|', 2) for l in lines if not l.startswith('#')]
    entries_norm = [[sura, aya, ' '.join(normaliser(text, cfg.get('normalisation', 'allowed')))]for sura, aya, text in entries]

quran_bigram_index = {}
quran_unigram_freq = {}
quran_length = {}
quran_verse_len_one = {}
quran_verse_len_two = {}

for sura, aya, text in entries_norm:

    sura_aya = (int(sura), int(aya))
    words = text.strip().split() #TODO perform normalization
    size = len(words)

    # calculate unigram frequencies
    for word in words:
        if word not in quran_unigram_freq:
            quran_unigram_freq[word] = (1, {sura_aya})
            
        else:
            quran_unigram_freq[word] = (quran_unigram_freq[word][0]+1,
                                        quran_unigram_freq[word][1].union({sura_aya}))

    if size >= 3:
        for i in range(size-1):
            pair = (words[i], words[i+1])

            quran_bigram_index[pair] = quran_bigram_index.get(pair, set()) | {sura_aya}

        quran_length[sura_aya] = size

    elif size == 2:
        pair = tuple(words)
        quran_verse_len_two[pair] = quran_verse_len_two.get(pair, set()) | {sura_aya}

    else:
        quran_verse_len_one[words[0]] = sura_aya


with open('quran_index.py', 'w') as outfp:

    print('quran_bigram_index = ', end='', file=outfp)
    pprint(quran_bigram_index, outfp)

    print('\nquran_unigram_freq = ', end='', file=outfp)
    pprint(quran_unigram_freq, outfp)

    print('\nquran_length = ', end='', file=outfp)
    pprint(quran_length, outfp)

    print('\nquran_verse_len_one = ', end='', file=outfp)
    pprint(quran_verse_len_one, outfp)

    print('\nquran_verse_len_two = ', end='', file=outfp)
    pprint(quran_verse_len_two, outfp)

