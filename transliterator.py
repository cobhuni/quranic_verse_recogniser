#!/usr/bin/python3.4
#
#     transliterator.py
#
# program to encode and decode strings into and from buckwalter transliteration
#
#################################################################################

from argparse import ArgumentParser,FileType
from sys import stdin,stdout


#
# arabic - buckwalter mapping
# source: http://corpus.quran.com/java/buckwalter.jsp
#

          # arabic : buckwalter # hex int letter_name
Table = {
          'ء' : '\'',  # U+0621 1569 Hamza
          'أ' : '>' ,  # U+0623 1571 Alif + HamzaAbove
          'ؤ' : '&' ,  # U+0624 1572 Waw + HamzaAbove
          'إ' : '<' ,  # U+0625 1573 Alif + HamzaBelow
          'ئ' : '}' ,  # U+0626 1574 Ya + HamzaAbove
          'ا' : 'A' ,  # U+0627 1575 Alif
          'ب' : 'b' ,  # U+0628 1576 Ba
          'ة' : 'p' ,  # U+0629 1577 TaMarbuta
          'ت' : 't' ,  # U+062A 1578 Ta
          'ث' : 'v' ,  # U+062B 1579 Tha
          'ج' : 'j' ,  # U+062C 1580 Jeem
          'ح' : 'H' ,  # U+062D 1581 HHa
          'خ' : 'x' ,  # U+062E 1582 Kha
          'د' : 'd' ,  # U+062F 1583 Dal
          'ذ' : '*' ,  # U+0630 1584 Thal
          'ر' : 'r' ,  # U+0631 1585 Ra
          'ز' : 'z' ,  # U+0632 1586 Zain
          'س' : 's' ,  # U+0633 1587 Seen
          'ش' : '$' ,  # U+0634 1588 Sheen
          'ص' : 'S' ,  # U+0635 1589 Sad
          'ض' : 'D' ,  # U+0636 1590 DDad
          'ط' : 'T' ,  # U+0637 1591 TTa
          'ظ' : 'Z' ,  # U+0638 1592 DTha
          'ع' : 'E' ,  # U+0639 1593 Ain
          'غ' : 'g' ,  # U+063A 1594 Ghain
          'ـ' : '_' ,  # U+0640 1600 Tatweel
          'ف' : 'f' ,  # U+0641 1601 Fa
          'ق' : 'q' ,  # U+0642 1602 Qaf
          'ك' : 'k' ,  # U+0643 1603 Kaf
          'ل' : 'l' ,  # U+0644 1604 Lam
          'م' : 'm' ,  # U+0645 1605 Meem
          'ن' : 'n' ,  # U+0646 1606 Noon
          'ه' : 'h' ,  # U+0647 1607 Ha
          'و' : 'w' ,  # U+0648 1608 Waw
          'ى' : 'Y' ,  # U+0649 1609 AlifMaksura
          'ي' : 'y' ,  # U+064A 1610 Ya
          'ً' : 'F' ,  # U+064B 1611 Fathatan
          'ٌ' : 'N' ,  # U+064C 1612 Dammatan
          'ٍ' : 'K' ,  # U+064D 1613 Kasratan
          'َ' : 'a' ,  # U+064E 1614 Fatha
          'ُ' : 'u' ,  # U+064F 1615 Damma
          'ِ' : 'i' ,  # U+0650 1616 Kasra
          'ّ' : '~' ,  # U+0651 1617 Shadda
          'ْ' : 'o' ,  # U+0652 1618 Sukun
          'ٓ' : '^' ,  # U+0653 1619 Maddah
          'ٔ' : '#' ,  # U+0654 1620 HamzaAbove
          'ٰ' : '`' ,  # U+0670 1648 AlifKhanjareeya
          'ٱ' : '{' ,  # U+0671 1649 Alif + HamzatWasl
          'ۜ' : ':' ,  # U+06DC 1756 SmallHighSeen
          '۟' : '@' ,  # U+06DF 1759 SmallHighRoundedZero
          '۠' : '"' ,  # U+06E0 1760 SmallHighUprightRectangularZero
          'ۢ' : '[' ,  # U+06E2 1762 SmallHighMeemIsolatedForm
          'ۣ' : ';' ,  # U+06E3 1763 SmallLowSeen
          'ۥ' : ',' ,  # U+06E5 1765 SmallWaw
          'ۦ' : '.' ,  # U+06E6 1766 SmallYa
          'ۨ' : '!' ,  # U+06E8 1768 SmallHighNoon
          '۪' : '-' ,  # U+06EA 1770 EmptyCentreLowStop
          '۫' : '+' ,  # U+06EB 1771 EmptyCentreHighStop
          '۬' : '%' ,  # U+06EC 1772 RoundedHighStopWithFilledCentre
          'ۭ' : ']'    # U+06ED 1773 SmallLowMeem
}


def translit(action, iterable, table=Table):
    """Generator that {de}transliterate an iterable of strings char by char.

    Args:
        action(str):    {encode,decode}
        iterable:       collection of strings
        table(dict):    buckwalter,latin mapping
    """

    # check for errors, for when it's called as module
    if action not in ('encode', 'decode'):
        try:
            raise NameError('InvalidAction')

        except NameError:
            print('Action not recognised, value must be {encode,decode}')
            raise
    
    # arabic character to buckwalter transliteration
    if action == 'decode':
        table = {v:k for k,v in table.items()}

    for line in iterable:
        for char in line:
            yield table.get(char,char)


if __name__ == "__main__":

    parser = ArgumentParser(description='encode and decode buckwalter transliteration')
    parser.add_argument('infile', nargs='?', type=FileType('r'), default=stdin, help='input text')
    parser.add_argument('outfile', nargs='?', type=FileType('w'), default=stdout, help='output text')
    parser.add_argument('action', nargs=1, choices=('encode', 'decode'), help='desired action'),
    args = parser.parse_args()
    
    for char in translit(args.action[0], args.infile):
        print(char, end='', file=args.outfile)