#!/bin/bash
#
#    get_data_quran.sh
#
# usage:
#   $ bash get_data_quran.sh 
#
####################################################

cat quran-simple.txt |
grep -v "#" |
grep -o . |
tr -d '0123456789|' |
grep . |
python -c "import sys
import unicodedata
for c in sys.stdin:
  c = c.strip()
  if c:
    print(c, hex(ord(c)), unicodedata.name(c), sep='\t')
" |
sort |
uniq -c |
sort -nr > xxx_quran-simple-charslist.txt


cat quran-simple.txt |
grep -v "#" |
awk -F "|" '{print $3}' |
tr ' ' '\n' |
grep . |
sort |
uniq -c |
sort -nr > xxx_quran-simple-wordfreqs.txt

