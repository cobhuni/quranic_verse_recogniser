#!/usr/bin/env python3
#
#    quranic_morphology_parser.py
#
# parse file quranic-corpus-morphology-0.4.txt and
# retrive relevant information
#
# dependencies:
#   * quranic-corpus-morphology-0.4.txt
#
# usage:
#   $ python 
#
####################################################

from operator import itemgetter
from itertools import groupby
from pprint import pprint #DEBUG

from transliterator import translit

with open('quranic-corpus-morphology-0.4.txt') as inf, \
    open('quranic-corpus-morphology-0.4_parsed.txt', 'w') as outf:

    lines = (l for l in filter(None, map(str.strip, inf)) if not l.startswith('#'))

    # skip header
    next(lines)

    data = (l.split('\t', 3) for l in lines)

    # unfold data for each entry
    entries = ({**dict(zip(('isura', 'iaya', 'itoken', 'isubtok'), index.strip('()').split(':',3))),
               'form' : ''.join(translit('decode', form)),
               'tag' : tag,
               'feat' : feat} for index, form, tag, feat in data)

    # group by token
    tokens = [list(g) for k,g in groupby(entries, itemgetter('itoken'))]

    for token in tokens:
        group = [subtok['feat'].partition('|')[0]=='STEM' for subtok in token]
        if len(group)==2 and all(group):
            pprint(token) #DEBUG
            print() #DEBUG
            #print('|'.join(subtok['tag'].partition('|')[0] for subtok in token)) #DEBUG

#isura|iaya|itoken|form|lem:tag;lem:tag

