#!/usr/bin/env python3
#
#    tester_quran_searcher.py
#
# TODO
# ----
#   * poner un warning cuando haya demasiados resultados (busquedas parciales)
#
##############################################################################

import os
import sys
import random
from argparse import ArgumentParser
from operator import itemgetter
from itertools import zip_longest

from quran_searcher import quran_searcher

ALLOWED_CHARSET = 'النمويهربتكعفقسدذحجىخةشصضزءآثطغئظؤأإ'

def create_typo(verse):
    """ Create a random typo verse of edit distance 1.

    Args:
        verse (str): text to modify.

    Return:
        str: modified verse.

    """
    size = len(verse)
    if size < 4: return verse

    DELETE, TRANSPOSE, REPLACE, INSERT = list(range(1,5))
    OPTION = random.choice([DELETE, TRANSPOSE, REPLACE, INSERT])

    # get position in which to add the typo, make sure it's not a space
    i = random.randint(2, size-2)
    while verse[i]==' ':
        i = random.randint(2, size-2)

    if OPTION == DELETE:
        return verse[:i]+verse[i+1:]

    elif OPTION == TRANSPOSE:
        return ''.join((verse[:i], verse[i+1], verse[i], verse[i+2:]))

    elif OPTION == REPLACE:
        return ''.join((verse[:i], random.choice(list(ALLOWED_CHARSET)), verse[i+1:]))

    #option INSERT
    return ''.join((verse[:i+1], random.choice(list(ALLOWED_CHARSET)), verse[i+1:]))

def overlap_ngrams(seq):
    """ Get overlapping groups of 3 items from seq. E.g.:
        if seq = (a, b, c, d, e), yield (a, b, c) (b, c, d) (c, d, e)

    Args:
        seq (iter): Sequence of items.

    Yield:
        tuple: overlapping groups.

    """
    try:
        fst, snd, trd = [next(seq) for _ in range(3)]

        while True:
            yield fst, snd, trd
            fst, snd, trd = snd, trd, next(seq)

    except StopIteration:
        pass


if __name__ == '__main__':

    parser = ArgumentParser(description='tester for quran_searcher.py')
    options = parser.add_mutually_exclusive_group(required=True)
    options.add_argument('--quran_complete', action='store_true', help='test against Quran (complete verse)')
    options.add_argument('--quran_complete_typo', action='store_true', help='test against Quran (complete verse with typo)')
    options.add_argument('--quran_partial', action='store_true', help='test against Quran (partial verse)')
    options.add_argument('--quran_partial_typo', action='store_true', help='test against Quran (partial verse with typo)')
    options.add_argument('--hadith', action='store_true', help='test against Hadith')
    args = parser.parse_args()

    if args.quran_complete:

        with open('quran-simple.txt') as infp:
            lines = filter(None, (l.strip() for l in infp))
            entries = (l.split('|',2) for l in lines if not l.startswith('#'))
        
            test_result = [((int(sura), int(aya)),
                            verse,
                            set(quran_searcher(verse, complete_verse=True))
                            ) for sura, aya, verse in entries]
        
        for sura_aya, verse, found_indexes in test_result:
        
            print('**', sura_aya, verse, found_indexes)
        
            length_found = len(found_indexes)
        
            if length_found == 0:
                print('ERROR! [0] ', sura_aya, verse, found_indexes, file=sys.stderr)
        
            elif length_found == 1:
                if sura_aya not in found_indexes:
                    print('ERROR! [!=]', sura_aya, verse, found_indexes, file=sys.stderr)
        
            else:
                if sura_aya not in found_indexes:
                    print('ERROR! [+]', sura_aya, verse, found_indexes, file=sys.stderr)
                
                #NOTE if complete_verse is True, this path means that the verses are repeated in quran
                #else:
                #    print(sura_aya, verse, found_indexes)


    if args.quran_complete_typo:

        with open('quran-simple.txt') as infp:
            lines = filter(None, (l.strip() for l in infp))
            entries = (l.split('|',2) for l in lines if not l.startswith('#'))

            entries_typo = ((sura, aya, verse, create_typo(verse)) for sura, aya, verse in entries)
        
            test_result = [((int(sura), int(aya)),
                            verse,
                            verse_typo,
                            set(quran_searcher(verse_typo, complete_verse=True))
                            ) for sura, aya, verse, verse_typo in entries_typo]
        
        for sura_aya, verse, verse_typo, found_indexes in test_result:
        
            print('**', sura_aya, verse, '|', verse_typo, found_indexes)
        
            length_found = len(found_indexes)
        
            if length_found == 0:
                print('ERROR! [0] ', sura_aya, verse, '|', verse_typo, found_indexes, file=sys.stderr)
        
            elif length_found == 1:
                if sura_aya not in found_indexes:
                    print('ERROR! [!=]', sura_aya, verse, '|', verse_typo, found_indexes, file=sys.stderr)
        
            else:
                if sura_aya not in found_indexes:
                    print('ERROR! [+]', sura_aya, verse, '|', verse_typo, found_indexes, file=sys.stderr)
                
                #NOTE if complete_verse is True, this path means that the verses are repeated in quran
                #else:
                #    print(sura_aya, verse, found_indexes)

    if args.quran_partial:

        with open('quran-simple.txt') as infp:
            lines = filter(None, (l.strip() for l in infp))
            entries = (l.split('|',2) for l in lines if not l.startswith('#'))

            groups = ((sura, aya, overlap_ngrams(iter(verse.split()))) for sura, aya, verse in entries)

            trigrams = ((aya, verse, tri) for aya, verse, gr in groups for tri in gr)
        
            test_result = [((int(sura), int(aya)),
                            tri,
                            set(quran_searcher(' '.join(tri), complete_verse=False))
                            ) for sura, aya, tri in trigrams]
        
        for sura_aya, verse, found_indexes in test_result:
        
            print('**', sura_aya, verse, found_indexes)
        
            length_found = len(found_indexes)
        
            if length_found == 0:
                print('ERROR! [0] ', sura_aya, verse, found_indexes, file=sys.stderr)
        
            elif length_found == 1:
                if sura_aya not in found_indexes:
                    print('ERROR! [!=]', sura_aya, verse, found_indexes, file=sys.stderr)
        
            else:
                if sura_aya not in found_indexes:
                    print('ERROR! [+]', sura_aya, verse, found_indexes, file=sys.stderr)
                
                #NOTE if complete_verse is True, this path means that the verses are repeated in quran
                #else:
                #    print(sura_aya, verse, found_indexes)


    if args.quran_partial_typo:

        with open('quran-simple.txt') as infp:
            lines = filter(None, (l.strip() for l in infp))
            entries = (l.split('|',2) for l in lines if not l.startswith('#'))

            groups = ((sura, aya, overlap_ngrams(iter(verse.split()))) for sura, aya, verse in entries)

            trigrams = ((aya, verse, tri) for aya, verse, gr in groups for tri in gr)
        
            test_result = [((int(sura), int(aya)),
                            tri,
                            set(quran_searcher(create_typo(' '.join(tri)), complete_verse=False)) #FIXME line changed for adding typo / without testing
                            ) for sura, aya, tri in trigrams]
        
        for sura_aya, verse, found_indexes in test_result:
        
            print('**', sura_aya, verse, found_indexes)
        
            length_found = len(found_indexes)
        
            if length_found == 0:
                print('ERROR! [0] ', sura_aya, verse, found_indexes, file=sys.stderr)
        
            elif length_found == 1:
                if sura_aya not in found_indexes:
                    print('ERROR! [!=]', sura_aya, verse, found_indexes, file=sys.stderr)
        
            else:
                if sura_aya not in found_indexes:
                    print('ERROR! [+]', sura_aya, verse, found_indexes, file=sys.stderr)
                
                #NOTE if complete_verse is True, this path means that the verses are repeated in quran
                #else:
                #    print(sura_aya, verse, found_indexes)

    if args.hadith:

        with open('hadith_all_tokens.txt') as infp:

            words = filter(None, map(str.strip, infp))

            for group in overlap_ngrams(iter(words)):
                indexes = list(quran_searcher(' '.join(group), complete_verse=False))

                if indexes:
                    print('FOUND!', ' '.join(group), indexes, file=sys.stderr)

